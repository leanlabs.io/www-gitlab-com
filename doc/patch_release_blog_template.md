---
layout: post
title: "GitLab X.X.X Released"
date: YYYY-MM-DD
comments: true
categories:
author: ADD_YOUR_FULL_NAME
---

Today we release GitLab X.X.X CE, EE and GitLab CI X.X.X.


GitLab X.X.X CE fixes:

- FIX_ONE
- FIX_TWO

In addition to the fixes in X.X.X CE, Enterprise Edition X.X.X contains the following fix(es):

- FIX_ONE
- FIX_TWO

GitLab X.X.X CI fixes:

- FIX_ONE
- FIX_TWO

<!-- more -->

## Upgrade barometer

*** DESCRIBE HOW INVOLVED THE MIGRATIONS ARE. CAN DEPLOY HAPPEN WITHOUT DOWNTIME? ***
*** CHECK IF THERE ARE ANY MIGRATIONS THAT REMOVE OR CHANGE COLUMNS. ***
*** IF THERE ARE ONLY ADDITIONS OR NO MIGRATIONS CONFIRM THAT DEPLOY CAN BE WITHOUT DOWNTIME ****

## Upgrading

Omnibus-gitlab packages for GitLab X.X.X are [now available](https://about.gitlab.com/downloads/).

To upgrade a GitLab installation from source please use the
[upgrader](http://doc.gitlab.com/ce/update/upgrader.html) or the [patch update
guide](http://doc.gitlab.com/ce/update/patch_versions.html).

To upgrade a GitLab CI installation from source, please use the [upgrade guide](https://gitlab.com/gitlab-org/gitlab-ci/blob/master/doc/update/patch_versions.md).

## Enterprise Edition

Omnibus packages for GitLab Enterprise Edition X.X.X are available for subscribers [here](https://gitlab.com/subscribers/gitlab-ee/blob/master/doc/install/packages.md). For installations from source, use [this guide](https://gitlab.com/subscribers/gitlab-ee/blob/master/doc/update/patch_versions.md).

Interested in GitLab Enterprise Edition?
For an overview of feature exclusive to GitLab Enterprise Edition please have a look at the [features exclusive to GitLab EE](http://about.gitlab.com/features/#enterprise).

Access to GitLab Enterprise Edition is included with a [subscription](http://www.gitlab.com/subscription/).
No time to upgrade GitLab yourself?
A subscription also entitles to our upgrade and installation services.
